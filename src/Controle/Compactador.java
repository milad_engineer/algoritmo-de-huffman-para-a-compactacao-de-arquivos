/*
 * UNIVERSIDADE CATÓLICA DOM BOSCO
 * TRABALHO DE ESTRUTURA DE DADOS II
 * PROFESSOR: MARCOS ALVES
 */
package Controle;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectOutputStream;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
/**
 * @data de início 17/05/2017 
 * @data de término 31/05/2017 
 * @author MILAD ROGHANIAN RA160985
 */
public class Compactador 
{
    Heap hp = new Heap();//Heap Local
    String code = new String();//String com texto de saída
    String text = new String();//Texto de entrada
    String hashcode[] = new String[256];//Código hash para armazenar os bits convertidos 
    //String teste = new String();//String para testes
    private String sbinout = new String();//série de binários de saída
    int vfreq[] = new int[256];//Vetor de frequência das letras
    int bitexc;//Quantidade de bits excedentes adicionados
    
    //########## LEITURA DO TXT ##################
    public void readtxt () 
    {
        try {
            JOptionPane.showMessageDialog(null,"ESCOLHA O TEXTO DE ENTRADA");
            JFileChooser chooserArquivo = new JFileChooser();
            int escolha = chooserArquivo.showOpenDialog(null);
            FileInputStream stream = new FileInputStream(chooserArquivo.getSelectedFile().getAbsolutePath());
            InputStreamReader reader = new InputStreamReader(stream);
            BufferedReader br = new BufferedReader(reader);
            String linha = br.readLine();
            while(linha!=null)    
            {
                this.text = this.text+linha+"\n";
                linha=br.readLine();
            }		
            br.close();
            JOptionPane.showMessageDialog(null,this.text);
        } catch (IOException e) {
            System.err.print("Erro na leitura compactação");
        }
    }
    
    //########## CONTAGEM DA FREQUÊNCIA DAS LETRAS ##################
    public void contChar()
    {
        for (int i=0; i<this.text.length(); i++)
        {
            this.vfreq[(int)this.text.charAt(i)]++;
        }
    }
    
    //########## CRIAÇÃO DO HEAP INSERINDO OS NOHUFFMAN ##################
    public void criaHeap()
    { 
        for (int i=0; i<255; i++)
        {
            if(this.vfreq[i]!=0){
                noHuffman huff = new noHuffman();
                char aux;
                aux = (char)i;
                huff.setLetra(Character.toString(aux));
                huff.setFrequencia(this.vfreq[i]);
                hp.insereHeap(huff);
            }        
        }
    }
    
    //########## CONCATENAÇÃO DOS NOHUFFMAN DO HEAP ATÉ SOBRAR 1 NO ##################
    public void concatenaHeap()
    {        
        while(hp.getN()>1)
        {
            noHuffman menor1,menor2;
            noHuffman hufftemp = new noHuffman();
            menor1 = hp.removeHeap();
            menor2 = hp.removeHeap();

            hufftemp.setFrequencia(menor1.getFrequencia()+menor2.getFrequencia());
            hufftemp.setLetra(null);
            hufftemp.setEsq(menor1);
            hufftemp.setDir(menor2);          
            hp.insereHeap(hufftemp);
        }
    }
    
    //########## PERCORRE A ÁRVORE HUFFMAN FAZENDO A CODIFICAÇÃO BINÁRIA ##################
    public void percorreHuff (noHuffman no, String stemp)
    {
        int x;
        if (no.getLetra()==null){
            percorreHuff(no.getEsq(),stemp+ "0");
            percorreHuff(no.getDir(),stemp+ "1");
        } else {
            x = (int)no.getLetra().charAt(0);
            this.hashcode[x] = stemp;
        }
    }
    
    //########## PERCORRE O TEXTO SUBSTITUINDO CADA LETRA PELA CODIFICAÇÃO EM BINÁRIO FEITA ACIMA ##################
    public void percorreString ()
    {
        int x;
        this.sbinout = new String();
        for (int i=0; i<this.text.length(); i++)
        {
            x = this.text.charAt(i);
            this.sbinout = this.sbinout + this.hashcode[x];
        }
    }
    
    //########## TRANSFORMA BINÁRIO EM INTEIRO ##################
    public int binaryToInteger(String binary) 
    {
        char[] numbers = binary.toCharArray();
        int result = 0;
        for(int i=numbers.length - 1; i>=0; i--)
            if(numbers[i]=='1')
                result += Math.pow(2, (numbers.length-i - 1));
        return result;
    }
    
    //########## TRANSFORMA CADA BYTE DA SEQUÊNCIA DE BINÁRIOS EM CARACTERES  ##################
    public void encode()
    {
        this.bitexc = this.sbinout.length() % 8;
        if (this.bitexc>0){
            this.bitexc = 8-this.bitexc;
        }
        for (int i=0; i<this.bitexc; i++)
        {
            this.sbinout = this.sbinout + "0";
        }
        
        int ascii;
        int i=0,j=0,k=0;
        String ajudante;
        while (i<this.sbinout.length()/8)
        {
            ajudante = new String();
            for (j=0; j<8; j++,k++)
            {
                ajudante = ajudante + this.sbinout.charAt(k);
            }
            
            ascii = binaryToInteger(ajudante);
            this.code = this.code + Character.toString((char)ascii);
            i++;
        }
    }
    
    //########## FAZ A SAÍDA DA CODIFICAÇÃO EM CARACTERES E DA ÁRVORE ##################
    public void outcode() throws FileNotFoundException
    {
        //Trabalhando em bits bufferizados
        File arquivo = new File("outcode.milad");
        try( FileOutputStream fo = new FileOutputStream( arquivo ) ){
            BufferedOutputStream bos = new BufferedOutputStream(fo);             
            bos.write(this.bitexc);
            bos.write(this.vfreq[10]);
            bos.write(this.code.getBytes());
            bos.flush();  
 
        }catch(IOException ex){
          ex.printStackTrace();
        }
      
        FileOutputStream outree = new FileOutputStream("outree.milad");
        noHuffman auxhuff = new noHuffman();
        try{
            ObjectOutputStream objectOut = new ObjectOutputStream(outree);
            auxhuff = hp.getChaves()[1];
            objectOut.writeObject(auxhuff);
            objectOut.flush();
            objectOut.close();
            outree.flush();
            outree.close();
        } catch(IOException e){
            //e.printStrackTrace();
        }
    }
    
    //########## MANIPULA TODAS AS FUNÇÕES DA CLASSE REALIZANDO A COMPACTAÇÃO ##################
    public void compactar() throws FileNotFoundException
    {
        readtxt();
        if (this.text.length()!=0){
            contChar();
            hp.inicializaHeap();
            criaHeap();
            concatenaHeap();
            percorreHuff(hp.buscaTopo(),this.sbinout);
            percorreString();
            encode();
            outcode();
            //System.out.println(hp.imprimeHeap(this.teste,1, 0));
            //System.out.println(trt.imprimeHuff(this.teste,trt.hp.buscaTopo(), 0));
            //System.out.println(this.sbinout);   
            //System.out.println("saída = "+this.code);
        }
    }
    
    //########## FUNÇÃO IMPRIME UM NÓ HUFFMAN ##################
    /*public String imprimeHuff (String saida,noHuffman no, int space)
    {
        if(no!=null) {//enquanto a arvore for diferente de nulo
            saida = imprimeHuff(saida,no.getEsq(),space+1); //entra na esquerda
            for (int i=1; i<=space; i++) //
            {
                saida += " - ";
            }
            saida = saida + "FRQ "+Integer.toString(no.getFrequencia())+"["+no.getLetra()+"]"+"\n"; //depois imprime o no
            saida = imprimeHuff(saida,no.getDir(),space+1);//e entra na direita
        }
        return saida;
    }*/
}











//@MILADROGHANIAN