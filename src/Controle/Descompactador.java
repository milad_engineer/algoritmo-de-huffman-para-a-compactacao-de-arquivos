/* FINAL
 * UNIVERSIDADE CATÓLICA DOM BOSCO
 * TRABALHO DE ESTRUTURA DE DADOS II
 * PROFESSOR: MARCOS ALVES
 */
package Controle;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectInputStream;
import static java.lang.Integer.toBinaryString;
import javax.swing.JOptionPane;
import java.io.Serializable;
import javax.swing.JFileChooser;
/**
 * @data de início 17/05/2017 
 * @data de término 31/05/2017 
 * @author MILAD ROGHANIAN RA160985
 */
public class Descompactador 
{
    String sbin = new String();//Armazena os binários
    Heap hpd = new Heap();//Heap local
    int restBin;//Binários extra adicionados
    int descont;//caracter que deve ser descontado
    
    
    //########## MANIPULA TODAS AS FUNÇÕES DA CLASSE REALIZANDO A DESCOMPACTAÇÃO ##################
    public void Descompactar() throws FileNotFoundException, ClassNotFoundException, IOException
    {
        String end = new String();
        
        hpd.inicializaHeap();
        
        JOptionPane.showMessageDialog(null,"ESCOLHA O CODIGO DE ENTRADA");
        JFileChooser chooserArquivo = new JFileChooser();
        int escolha = chooserArquivo.showOpenDialog(null);
        end = chooserArquivo.getSelectedFile().getAbsolutePath();
        if (end.length()!=0){
            textReader(end);
            end = new String();
            JOptionPane.showMessageDialog(null,"ESCOLHA A ARVORE");
            escolha = chooserArquivo.showOpenDialog(null);
            end = chooserArquivo.getSelectedFile().getAbsolutePath();
        }
        if (end.length()!=0){
            treeReader(end);
            treetoText(hpd.buscaTopo());
            File arquivo = new File("saida.txt");
            FileWriter fw = new FileWriter(arquivo);           
            fw.write(this.outext);
            fw.flush();  
            JOptionPane.showMessageDialog(null,outext);
        }  
    }
    
    //########## ADICIONA "ZEROS" PARA CADA CHAR COMPLETAR 8 BITS=1BYTE ##################
    public String binario(int intascii)
    {
        String bin = new String();
        bin = toBinaryString(intascii);
        while (bin.length()<8)
        {
            bin = "0" + bin;
        }
        return bin;
    }
    
    //########## FAZ A LEITURA DO CÓDIGO DE ENTRADA RETIRANDO OS BITS RESTANTES ##################
    public void textReader(String end)
    {
        int i=0;
        
        //DA STRING DE TEXTO 
        try{ 
                File stream = new File (end);
                FileReader reader = new FileReader(stream);
                int aux = reader.read();
                this.restBin = aux;
                aux = reader.read();
                this.descont = aux;
                aux = reader.read();
                while(aux!=-1)    
                {                    
                    this.sbin = this.sbin+binario((int)aux);
                    aux = reader.read();
                }		
                reader.close();
            } catch (IOException e) {
                System.err.print("Erro na leitura descompactação");
            }
        //RETIRANDO BITS QUE SOBRARAM 
        if (this.restBin>0){
            String temp = new String();
            int newtam = this.sbin.length() - this.restBin;
            
            for (i=0; i<newtam; i++)
            {
                temp = temp + this.sbin.charAt(i);
            }
            this.sbin=temp;
        }
    }
    
    //########## REALIZA A LEITURA DA ÁRVORE HUFFMAN INSERINDO-A NO HEAP LOCAL ##################
    public void treeReader(String end) throws FileNotFoundException, ClassNotFoundException
    {
        noHuffman auxhuff = new noHuffman();
        
        try{
            FileInputStream intree = new FileInputStream(end);
            ObjectInputStream ois = new ObjectInputStream(intree);    
            auxhuff = (noHuffman)ois.readObject();
            hpd.insereHeap(auxhuff);
            ois.close();
            intree.close();
        } catch(IOException e){
            //e.printStrackTrace();
        }   
    }
    
    //########## MANIPULA A "LETTERTOTEXT" PEGANDO LETRA POR LETRA ##################
    int cont=0;//Realiza a contagem da string de binários até o final
    String outext = new String();//armazena o texto de saída
    public void treetoText(noHuffman raiz)
    {
        while (cont<this.sbin.length()-(this.descont+this.descont))
        {
            lettertoText(raiz);
        }
    }
    
    //########## PEGA LETRA POR LETRA DA ÁRVORE ##################
    public void lettertoText (noHuffman no)
    {
        if (this.sbin.charAt(cont)=='0' && no.getLetra()==null){
            cont++;
            lettertoText(no.getEsq());
        } else if (this.sbin.charAt(cont)=='1' && no.getLetra()==null){
            cont++;
            lettertoText(no.getDir());
        } else if (no.getLetra()!=null){
            outext = outext + no.getLetra();
        }
    }
}
