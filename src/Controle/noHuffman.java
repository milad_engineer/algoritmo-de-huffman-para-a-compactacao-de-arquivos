/*
 * UNIVERSIDADE CATÓLICA DOM BOSCO
 * TRABALHO DE ESTRUTURA DE DADOS II
 * PROFESSOR: MARCOS ALVES
 */
package Controle;
import java.io.Serializable;
/**
 * @data de início 17/05/2017 
 * @data de término 31/05/2017 
 * @author MILAD ROGHANIAN RA160985
 */
public class noHuffman implements Serializable
{
    private String letra;
    private int frequencia;
    private noHuffman esq;
    private noHuffman dir;


    /**
     * @return the frequencia
     */
    public int getFrequencia() {
        return frequencia;
    }

    /**
     * @param frequencia the frequencia to set
     */
    public void setFrequencia(int frequencia) {
        this.frequencia = frequencia;
    }

    /**
     * @return the esq
     */
    public noHuffman getEsq() {
        return esq;
    }

    /**
     * @param esq the esq to set
     */
    public void setEsq(noHuffman esq) {
        this.esq = esq;
    }

    /**
     * @return the dir
     */
    public noHuffman getDir() {
        return dir;
    }

    /**
     * @param dir the dir to set
     */
    public void setDir(noHuffman dir) {
        this.dir = dir;
    }

    /**
     * @return the letra
     */
    public String getLetra() {
        return letra;
    }

    /**
     * @param letra the letra to set
     */
    public void setLetra(String letra) {
        this.letra = letra;
    }
    
    
}
