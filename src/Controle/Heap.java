/*
 * UNIVERSIDADE CATÓLICA DOM BOSCO
 * TRABALHO DE ESTRUTURA DE DADOS II
 * PROFESSOR: MARCOS ALVES
 */
package Controle;
/**
 * @data de início 17/05/2017 
 * @data de término 31/05/2017 
 * @author MILAD ROGHANIAN RA160985
 */
public class Heap 
{
    private noHuffman chaves[];
    private int n=0;
    
    public void inicializaHeap()
    {
        this.chaves = new noHuffman[255];
    }
    
    public void insereHeap (noHuffman chave)
    {
        if (this.n<255){
            this.n=this.n+1;
            this.chaves[this.n]=new noHuffman();
            this.chaves[this.n]=chave;
            subida(this.n);
        } 
    }
    
    public void subida(int Tchaves)
    {
        if (Tchaves>1){
            int p = Tchaves/2;
            if (this.getChaves()[p].getFrequencia()>this.getChaves()[Tchaves].getFrequencia()){
                noHuffman aux;
                aux=this.getChaves()[p];
                this.chaves[p]=this.getChaves()[Tchaves];
                this.chaves[Tchaves]=aux;
                subida(p);
            }
        }
    }
    
    public noHuffman removeHeap()
    {
        noHuffman temp;
        temp = this.chaves[1];
        this.chaves[1]=this.chaves[this.n];
        this.chaves[this.n]=null;
        this.n=this.n-1;
        if(this.n>1){
            descida(1);
        }
    return temp;
    }
    
    public void descida(int i)
    {
        int f = 2*i;
        if(f<=this.n){
            if((f+1)<=this.n && this.chaves[f].getFrequencia()>this.chaves[f+1].getFrequencia()){
                f=f+1;
            }
            if(this.chaves[i].getFrequencia()>this.chaves[f].getFrequencia()){
                noHuffman aux;
                aux=this.chaves[i];
                this.chaves[i]=this.chaves[f];
                this.chaves[f]=aux;
                descida(f);
            }
        }
    }
    
    public noHuffman buscaTopo()
    {
        return this.chaves[1];
    }
    
    public String imprimeHeap (String saida, int pos, int space)
    {
        if (pos<=this.n) {//enquanto a arvore for diferente de nulo
            saida = imprimeHeap(saida,2*pos+1,space+1); //entra na esquerda
            for (int i=1; i<=space; i++) //
            {
                saida += " - ";
            }
            saida += "FRQ = "+Integer.toString(this.chaves[pos].getFrequencia())+"["+this.chaves[pos].getLetra()+"]"+"\n"; //depois imprime o no
            saida = imprimeHeap(saida,2*pos,space+1);//e entra na direita
        }
        return saida;
    }

    /**
     * @return the chaves
     */
    public noHuffman[] getChaves() {
        return chaves;
    }

    /**
     * @return the n
     */
    public int getN() {
        return n;
    }
}
