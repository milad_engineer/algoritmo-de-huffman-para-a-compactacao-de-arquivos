Em java temos duas formas de tratar arquivos, como um fluxo baseado em bytes 
armazenando seus valores em seu formato binario, por exemplo se formos armazenar o 
numero 25 em arquivo em um fluxo de bytes sera armazenado em sua forma binaria que 
seria 00011001. Ou podemos armazenar o valor 25 como um 
fluxo baseado em caracteres onde cada um dos caracteres e armazenado utilizando o 
valor unicode dele, ou seja, o caractere 2 e representado pelo valor 50 na tabela 
unicode e que em binario ficaria 0000000000110010 e o caractere 5 e representado 
por 53 que em binario ficaria 0000000000110101, entao para armazenar o valor 25 
em um arquivo baseado em caracteres seria armazenado 
00000000001100100000000000110101 a diferenca dos dois nao e simplesmente o 
tamanho, o primeiro 25 armazenado em binario e um inteiro e pode ser utilizado 
para um calculo sem nenhuma conversao, o 25 armazenado como caractere foi 
armazenado como string e para todos os efeitos e um texto, tanto que podemos abrir 
um arquivo baseado em caractere em qualquer editor de texto e visualizar as 
informacoes, o mesmo nao e possivel utilizando um arquivo baseado e bytes que 
necessita de um programa especial que converta suas informacoes para um formato 
legivel para pessoas normais
tamanho, o primeiro 25 armazenado em binario e um inteiro e pode ser utilizado 
para um calculo sem nenhuma conversao, o 25 armazenado como caractere foi 
armazenado como string e para todos os efeitos e um texto, tanto que podemos abrir 
um arquivo baseado em caractere em qualquer editor de texto e visualizar as 
informacoes, o mesmo nao e possivel utilizando um arquivo baseado e bytes que 
necessita de um programa especial que converta suas informacoes para um formato 
legivel para pessoas normais